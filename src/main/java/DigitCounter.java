import java.util.ArrayList;
import java.util.Collections;

public class DigitCounter {

    private int number;

    DigitCounter(int number){
        this.number = number;
    }

    public  int countDigit(int digitToCount) {
        if (digitToCount < 0 || digitToCount > 9){
            return -1;
        }
        int count = 0;
        int rest = Math.abs(number);
        while (rest != 0) {
            int actualDigit = rest % 10;
            if (actualDigit == digitToCount) {
                count++;
            }
            rest /= 10;
        }
        return count;
    }

    public boolean containsDigit(int digit){
        return countDigit(digit) > 0;
    }

}
